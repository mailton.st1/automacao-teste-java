#language: pt

Funcionalidade: Pesuisar no Google

  Contexto: Como usuário desejo fazer uma pesquisa de uma página no google e acessar o site

      @wip
      Esquema do Cenario: Pesquisa no Google
        Dado que acesse o site <site>
        E pesquiso por <pesquisa> no campo de pesquisa
        Quando acesso o site após selecionar um dos resultados da pesquisa
        Então devo ver a pagina inicial

        Exemplos:
          |   site                        |   pesquisa          |
          |   https://www.google.com.br/  |   mercado bitcoin   |