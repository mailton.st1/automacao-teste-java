package br.com.automacaojava.pageobjects;

import br.com.automacaojava.utils.Element;
import org.openqa.selenium.By;

public class BasePage extends Element {

    static void preencherInput(By by, String text) {
        waitElement(by);
        element(by).sendKeys(text);
    }

    static void click(By by) {
        waitElement(by);
        element(by).click();
    }

    protected static String getText(By by) {
        return element(by).getText();
    }

    protected static void selecinoTab(By by){
        waitElement(by);
        element(by).sendKeys("\t");
    }
}
