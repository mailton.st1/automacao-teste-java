package br.com.automacaojava.pageobjects;

import br.com.automacaojava.utils.Browser;
import org.openqa.selenium.By;

public class PesquisaGooglePage extends BasePage {

    private static final By campoPesquisa = By.cssSelector(
            "body > div.L3eUgb > div.o3j99.ikrT4e.om7nvf > form > div:nth-child(2) > div.A8SBwf > div.RNNXgb > div > div.a4bIc > input");
    private static final By btnPesquisar = By.cssSelector("center:nth-child(1) input:first-child");

    private static final By resultadoPesquisa = By
            .cssSelector("a[href='https://www.mercadobitcoin.com.br/'] h3:nth-child(2)[class='LC20lb DKV0Md']");
    private static final By textPaginaInicial = By
            .cssSelector("body > section.hero > div > div.text-content > p:nth-child(2)");

    public static void acessarGoogle(String site) {
        Browser.loadPage(site);
    }

    public static void preencherCampoPesquisa(String pesquisa) {
        preencherInput(campoPesquisa, pesquisa);
    }

    public static void clicarBtnPesquisa() {
        click(btnPesquisar);
    }

    public static void clicarResultadoPesquisa() {
        click(resultadoPesquisa);
    }

    public static String validarTextoNaPaginaInicial() {
        return element(textPaginaInicial).getText();
    }

}
