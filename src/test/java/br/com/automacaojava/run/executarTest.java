package br.com.automacaojava.run;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(dryRun = false, strict = true, format = { "pretty",
        "html:target/cucunber-html-report" }, features = "src/resources/features", glue = "br/com/automacaojava/steps", tags = {
                "@wip" }, plugin = { "json:target/cucumber.json" })
public class executarTest {

}
