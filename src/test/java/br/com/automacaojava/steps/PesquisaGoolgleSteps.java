package br.com.automacaojava.steps;

import br.com.automacaojava.pageobjects.PesquisaGooglePage;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import cucumber.api.java8.Pt;
import org.junit.Assert;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PesquisaGoolgleSteps extends BaseSteps implements Pt {

    public static String resultadoObtido = "Acesso, segurança e diversificação para você e mais de 2 milhões de clientes.";

    @Dado("^que acesse o site (.*)$")
    public void queAcessoSite(String site) {
        PesquisaGooglePage.acessarGoogle(site);
    }

    @E("^pesquiso por (.*) no campo de pesquisa$")
    public void pesquisoPorSiteNoCampoDePesquisa(String site) {
        PesquisaGooglePage.preencherCampoPesquisa(site);
    }

    @Quando("^acesso o site após selecionar um dos resultados da pesquisa$")
    public void acessoOSiteApósSelecionarUmDosResultadosDaPesquisa() {
        PesquisaGooglePage.clicarBtnPesquisa();
        PesquisaGooglePage.clicarResultadoPesquisa();
    }

    @Entao(value = "^devo ver a pagina inicial$")
    public void devoVerAPaginaInicial() {
        Assert.assertEquals(PesquisaGooglePage.validarTextoNaPaginaInicial(), resultadoObtido);
    }
}
