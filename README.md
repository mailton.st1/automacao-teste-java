## Automacao-java:: Automação de testes funcionais
Documentação:

Projeto de automação de testes desenvolvido em Java, Cucumber  e Selenium para ser usado como projeto basico.

### Pré-Requisito:

* Ter o `java 8` configurado na máquina
   * Rode o comando `javac -version` para garantir.
   
### Preprarar o ambiente:

* Importar o projeto maven

#### Execução do projeto:

```IDE intellij:```
* Para rodar os testes, execute o arquivo ```executarTest``` na pasta ```run``` ou execute o comando ```mvn test``` na raiz do projeto.
***
```IDE Visual Code:```
* Execute o comando na raiz do projeto: ```mvn test```
***

#### Organização do projeto:

```Pasta feature:```
* Fica armazenado as features com os cenários.

```
Organização do projeto.

* driver (Armazena o chromeDriver na <versão> do browser local)
* src
  /resources
    * features (Armazena os cenários em BDD)
  /test/java/br.com.automacaojava
    * pageObject (Class com os mapeamento dos elementos por pagina)
    * run (Class para rodar os testes)
    * steps (Class com os passos)
    * utils (Class que inicia o Browser)

```

#### Cenário:

Escrita do cenário em BDD, utilizando o framework cucumber.
```
Funcionalidade: Pesuisar no Google

  Contexto: Como usuário desejo fazer uma pesquisa de uma página no google e acessar o site

      @wip
      Esquema do Cenario: Pesquisa no Google
        Dado que acesse o site <site>
        E pesquiso por <pesquisa> no campo de pesquisa
        Quando acesso o site após selecionar um dos resultados da pesquisa
        Então devo ver a pagina inicial

        Exemplos:
          |   site                        |   pesquisa          |
          |   https://www.google.com.br/  |   mercado bitcoin   |

```

###### Autor: 
```Mailton Nascimento```
